import React from 'react';
import './App.css';
import Left from "./components/left/left.component";
import Right from "./components/right/right.component";

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <Left/>
                <Right/>
            </header>
        </div>
    );
}

export default App;
