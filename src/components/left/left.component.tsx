import {FunctionComponent} from "react";
import {useAppDispatch} from "../../store/hooks";
import {addItem} from "../../store/shopping-cart/shoppingCartSlice";

const allItems = [
    {
        id: 1,
        title: "Product 1",
        price: 100
    },
    {
        id: 2,
        title: "Product 2",
        price: 200
    },
    {
        id: 3,
        title: "Product 3",
        price: 300
    }
]

const Left: FunctionComponent = () => {
    const dispatch = useAppDispatch()
    return (
        <>
            <h1>Left Component</h1>
            <button
                onClick={
                    () => dispatch(addItem(allItems[0]))
                }
            >
                Add Random item
            </button>
        </>
    )
}

export default Left
