import {FunctionComponent} from "react";
import {useSelector} from "react-redux";
import {shoppingCartSelector} from "../../store/shopping-cart/shoppingCartSlice";

const Right: FunctionComponent = () => {
    const selector = useSelector(shoppingCartSelector)

    return (
        <>
            <h1>Right Component</h1>
            <div>Total items count: {selector.shoppingCart.items.length}</div>
            {
                selector.shoppingCart.items.map((it, idx) => {
                    return (
                        <div key={idx}>
                            <h2>{it.id}</h2>
                            <div>{it.title}</div>
                            <div>{it.price}</div>
                        </div>
                    )
                })
            }
        </>

    )
}

export default Right
