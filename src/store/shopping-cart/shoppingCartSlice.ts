import {createSlice, Draft, PayloadAction} from '@reduxjs/toolkit'
import {RootState} from "../store";

interface ShoppingCartItem {
    id: number
    title: string
    price: number
}

interface ShoppingCart {
    items: ShoppingCartItem[]
}

const initialState = () => {
    return {
        items: new Array<ShoppingCartItem>()
    } as ShoppingCart
};

export const shoppingCartSlice = createSlice({
    name: "shopping-cart",
    initialState: initialState,
    reducers: {
        addItem: (state: Draft<ShoppingCart>, action: PayloadAction<ShoppingCartItem>) => {
            state.items = [...state.items, action.payload]
        },
    }
})

export const {addItem} = shoppingCartSlice.actions

// Other code such as selectors can use the imported `RootState` type
export const shoppingCartSelector = (state: RootState) => state

export default shoppingCartSlice.reducer
