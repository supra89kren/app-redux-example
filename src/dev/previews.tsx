import React from "react";
import {ComponentPreview, Previews} from "@react-buddy/ide-toolbox";
import {PaletteTree} from "./palette";
import Left from "../components/left/left.component";

const ComponentPreviews = () => {
    return (
        <Previews palette={<PaletteTree/>}>
            <ComponentPreview path="/Left">
                <Left/>
            </ComponentPreview>
        </Previews>
    );
};

export default ComponentPreviews;
